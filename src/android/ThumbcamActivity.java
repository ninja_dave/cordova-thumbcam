package ninja.davewolfe.thumbcam;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class ThumbcamActivity extends Activity {

    private static final String TAG = "ThumbcamActivity";

    private int size;
    private String fileName;
    private Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res = getResources();
        String pkg = getPackageName();
        setContentView(res.getIdentifier("activity_thumbcam", "layout", pkg));

        Intent intent = getIntent();
        size = intent.getIntExtra("size", 256);
        fileName = intent.getStringExtra("fileName");
        if (fileName == null || fileName.isEmpty())
            fileName = "thumbjpg";

        SurfaceView preview = new SurfaceView(this);
        SurfaceHolder surfaceHolder = preview.getHolder();
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    camera = getCamera();
                    camera.setPreviewDisplay(holder);
                } catch (Exception e) {
                    Log.e(TAG, "Error setting camera preview display: " + e.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (holder.getSurface() == null) {
                    return;
                }
                // stop preview before making changes
                try {
                    camera.stopPreview();
                } catch (Exception e) {
                    //ignore; tried to stop a non-existent preview
                }
                try {
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();
                } catch (Exception e) {
                    Log.d(TAG, "Error starting camera preview: " + e.getMessage());
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                releaseCamera();
            }
        });

        FrameLayout fl = (FrameLayout)findViewById(res.getIdentifier("camera_preview", "id", pkg));
        fl.addView(preview);

        final RelativeLayout root = (RelativeLayout)findViewById(res.getIdentifier("root", "id", pkg));
        final LinearLayout frameTop = (LinearLayout)findViewById(res.getIdentifier("frame_top", "id", pkg));
        final LinearLayout frameBtm = (LinearLayout)findViewById(res.getIdentifier("frame_bottom", "id", pkg));
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    root.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                int height = (root.getHeight() - root.getWidth()) / 2;
                frameBtm.getLayoutParams().height = height;
                frameTop.getLayoutParams().height = height;
            }
        });

        Button capture = (Button)findViewById(res.getIdentifier("btn_capture", "id", pkg));
        Button cancel = (Button)findViewById(res.getIdentifier("btn_cancel", "id", pkg));

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera != null) {
                    try {
                        camera.takePicture(null, null, new Camera.PictureCallback() {
                            @Override
                            public void onPictureTaken(byte[] data, Camera camera) {
                                new ProcessPhotoTask().execute(data);
                            }
                        });
                    } catch (Exception e) {
                        Log.e(TAG, "Error taking picture: " + e.getMessage());
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "cancelled :(");
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void finish() {
        releaseCamera();
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null)
            camera = getCamera();
        if (camera != null)
            camera.startPreview();
    }

    @Override
    protected void onPause() {
        releaseCamera();
        super.onPause();
    }

    @Override
    protected void onStop() {
        releaseCamera();
        super.onStop();
    }

    private Camera getCamera() {
        try {
            releaseCamera();
            int id = getBackCameraId();
            Camera cam = Camera.open(id);
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(id, info);
            cam.setDisplayOrientation(info.orientation);
            Camera.Parameters parameters = cam.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            cam.setParameters(parameters);
            return cam;
        } catch (Exception e) {
            Log.e(TAG, "Error getting camera: " + e.getMessage());
            setResult(RESULT_CANCELED);
            finish();
        }
        return null;
    }

    private int getBackCameraId() {
        int camCount = Camera.getNumberOfCameras();
        Camera.CameraInfo info = new Camera.CameraInfo();
        int id;
        for (id = 0; id < camCount; id++) {
            Camera.getCameraInfo(id, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                return id;
        }
        return id;
    }

    private void releaseCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    class ProcessPhotoTask extends AsyncTask<byte[], Void, String> {
        @Override
        protected String doInBackground(byte[]... data) {
            byte[] bytes = data[0];
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            int s = bitmap.getHeight();
            int x = (bitmap.getWidth() - s) / 2;
            bitmap = Bitmap.createBitmap(bitmap, x, 0, s, s, matrix, false);
            bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
            Log.i(TAG, "bitmap created: " + bitmap.getWidth() + "x" + bitmap.getHeight());

            File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            if (dir != null && !dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.d(TAG, "Failed to create photo directory");
                    return null;
                }
            }
            File file = new File(dir + File.separator + fileName);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                return file.getAbsolutePath();
            } catch (Exception e) {
                Log.d(TAG, "Error saving file: " + e.getMessage());
            } finally {
                try {
                    if (out != null)
                        out.close();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "postExecute: " + result);
            if (result ==  null || result.isEmpty()) {
                setResult(RESULT_CANCELED);
            } else {
                Intent intent = new Intent();
                intent.putExtra("path", result);
                setResult(RESULT_OK, intent);
            }
            finish();
        }
    }
}
