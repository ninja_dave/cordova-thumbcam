package ninja.davewolfe.thumbcam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

public class Thumbcam extends CordovaPlugin {

    private static final String TAG = "Thumbcam";

    private CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("takePicture")) {
            this.callbackContext = callbackContext;
            String fileName = args.getString(0);
            int size = args.getInt(1);
            this.takePicture(fileName, size);
            return true;
        }
        return false;
    }

    private void takePicture(String fileName, int size) {
        Log.i(TAG, "fileName: " + fileName + ", size: " + size);
        Context context = cordova.getActivity().getApplicationContext();
        Intent intent = new Intent(context, ThumbcamActivity.class);
        intent.putExtra("size", size);
        intent.putExtra("fileName", fileName);
        cordova.startActivityForResult(this, intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            Log.i(TAG, "Thumbcam ok!");
            String path = "file://" + intent.getStringExtra("path");
            callbackContext.success(path);
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i(TAG, "Thumbcam cancelled");
            callbackContext.error("Cancelled :(");
        } else {
            Log.i(TAG, "something went really wrong");
            callbackContext.error("Thumbcam Failed, reason unknown");
        }
    }
}
