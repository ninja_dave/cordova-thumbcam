//
//  Thumbcam.swift
//  ThumbcamApp
//
//  Created by Dave Wolfe on 8/2/15.
//
//

import Foundation

@objc(Thumbcam)
class Thumbcam: CDVPlugin {
    
    func takePicture(command: CDVInvokedUrlCommand) {
        println("takePicture")
        let name = command.arguments[0] as! String
        let size = command.arguments[1] as! Int
        
        let tvc = ThumbcamViewController()

        tvc.handleCancel = {() -> Void in
            tvc.picker?.dismissViewControllerAnimated(true, completion: nil)
            let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: "Cancelled")
            self.commandDelegate.sendPluginResult(pluginResult, callbackId:command.callbackId)
        }
        
        tvc.handleSuccess = {(image:UIImage) -> Void in
            tvc.picker?.dismissViewControllerAnimated(true, completion: nil)
            
            // resize image
            let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: size))
            iv.contentMode = UIViewContentMode.ScaleAspectFill
            iv.image = image
            UIGraphicsBeginImageContext(iv.bounds.size)
            iv.layer.renderInContext(UIGraphicsGetCurrentContext())
            let result = UIGraphicsGetImageFromCurrentImageContext()
            println(result)
            UIGraphicsEndImageContext()
            
            // save file to Documents directory
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
            let docDir = paths[0] as! String
            let path = docDir.stringByAppendingPathComponent(name)
            let data = UIImageJPEGRepresentation(result, 0.9)
            data.writeToFile(path, atomically: true)
            let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "file://" + path)
            self.commandDelegate.sendPluginResult(pluginResult, callbackId:command.callbackId)
        }
        
        self.viewController.presentViewController(tvc.picker!, animated: true, completion: nil)
    }
}