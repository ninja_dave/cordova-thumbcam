//
//  ThumbcamViewController.swift
//  ThumbcamApp
//
//  Created by Dave Wolfe on 8/4/15.
//
//

import UIKit

class ThumbcamViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    var picker:UIImagePickerController?
    var handleSuccess:((image:UIImage) -> Void)?
    var handleCancel:(() -> Void)?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initPicker()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initPicker()
    }
    
    convenience init() {
        self.init(nibName: "ThumbcamView", bundle: nil)
    }
    
    func initPicker() {
        let bounds = UIScreen.mainScreen().bounds
        view.frame = bounds
        view.backgroundColor = UIColor.clearColor()
        picker = UIImagePickerController()
        picker?.sourceType = UIImagePickerControllerSourceType.Camera
        picker?.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo
        picker?.cameraDevice = UIImagePickerControllerCameraDevice.Rear
        picker?.allowsEditing = false
        picker?.showsCameraControls = false
        picker?.view.frame = view.frame
        let ratio:CGFloat = 4/3
        let camHeight = bounds.width * ratio
        let yPos = (bounds.height - camHeight) / 2
        let transform = CGAffineTransformMakeTranslation(0, yPos)
        picker?.cameraViewTransform = transform
        picker?.cameraOverlayView = view
        picker?.delegate = self
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientation.Portrait.rawValue
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let bounds = UIScreen.mainScreen().bounds
        let height = (bounds.height - bounds.width) / 2
        topConstraint.constant = height
        bottomConstraint.constant = height
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.handleSuccess = nil;
        self.handleCancel = nil;
    }
    
    //MARK: delegates
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        handleSuccess?(image: image)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        handleCancel?()
    }
    
    //MARK: actions

    @IBAction func handleCapture(sender: UIButton) {
        picker?.takePicture()
        if (nil == self.picker) {
            handleCancel?()
        }
    }
    
    @IBAction func handleCancel(sender: UIButton) {
       handleCancel?()
    }
}
