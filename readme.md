Thumbcam
========
A Cordova plugin for taking square thumbnail photos.  Compatible with iOS and Android.  

To install:  
`cordova plugin add https://ninja_dave@bitbucket.org/ninja_dave/cordova-thumbcam.git`

Usage:  
```javascript
thumbcam.takePicture("thumb.jpg", 512, function(path){
	console.log("thumbnail saved to " + path);
}, function(error){
	console.log("error: " + error);
});
```