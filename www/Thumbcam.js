var exec = require('cordova/exec');

exports.takePicture = function(name, size, success, error) {
    if (!name) name = "thumb.jpg";
    size = isNaN(size) ? 256 : Math.min(1024, Math.max(16, size));
    exec(success, error, "Thumbcam", "takePicture", [name, size]);
};